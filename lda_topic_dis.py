import cPickle
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize
from nltk.stem.porter import PorterStemmer
import re
'''Topic distribution of all posts
'''
space = ' '
from stop_words import get_stop_words

en_stop = get_stop_words('en')
p_stemmer = PorterStemmer()
htmlentities = ["&quot;","&nbsp;","&amp;","&lt;","&gt;","&OElig;","&oelig;","&Scaron;","&scaron;","&Yuml;","&circ;","&tilde;","&ensp;","&emsp;","&thinsp;","&zwnj;","&zwj;","&lrm;","&rlm;","&ndash;","&mdash;","&lsquo;","&rsquo;","&sbquo;","&ldquo;","&rdquo;","&bdquo;","&dagger;","&Dagger;","&permil;","&lsaquo;"]
validElement = [',','.','"','\'']

def process(original_text):
    clean_text = ""
    for eachone in original_text:
        try:
            eachone.encode('ascii')
            clean_text += eachone
        except:
            pass
    return clean_text

def sentencePreProcess(raw):
    raw = raw.encode('ascii','ignore')
    for h in htmlentities:
        raw = raw.replace(h, " ")
    tokens = word_tokenize(raw.lower())
    stopped_tokens = [j for j in tokens if not j in en_stop]
    valid_tokens = [j for j in stopped_tokens if (j.isalpha() or j in validElement)]
    stemmed_tokens = [p_stemmer.stem(j) for j in valid_tokens]
    return space.join(stemmed_tokens)

def preProcess(plainText):
    plainText = re.sub(u'``',' " ',plainText)
    plainText = re.sub(u'\'\'',' " ',plainText)
    plainText = re.sub(u'\|','',plainText)
    allsentences = sent_tokenize(plainText)
    afterProcess = ""
    for i in range(len(allsentences)):
        if i != len(allsentences)-1:
            tempSentence = sentencePreProcess(allsentences[i])
            afterProcess += tempSentence + ' '
    return afterProcess

# load positive data
reader = open('labeled_reddit_pos.txt','r')
all_text = ''
for eachline in reader:
    all_text += eachline
all_subs = all_text.split('====')
pos = []
for eachsub in all_subs:
    clean_sub = preProcess(process(eachsub))
    pos.append(clean_sub)

# load positive data
reader = open('labeled_reddit_neg.txt','r')
all_text = ''
for eachline in reader:
    all_text += eachline
all_subs = all_text.split('====')
neg = []
for eachsub in all_subs:
    clean_sub = preProcess(process(eachsub))
    neg.append(clean_sub)
LDA_model = cPickle.load(open('lda_model_6k', 'rb'))
dictionary = cPickle.load(open('lda_dic_6k', 'rb'))
totalArticle_token = [eacharticle.split() for eacharticle in pos+neg]
corpus = [dictionary.doc2bow(article) for article in totalArticle_token]
totalTopicResults = []
all_topics = LDA_model.get_document_topics(corpus)
for doc_topics in all_topics:
    maxProb = -1
    desig = -1
    for eachone in doc_topics:
        (topicNumber, prob) = eachone
        if prob > maxProb:
            maxProb = prob
            desig = topicNumber
    totalTopicResults.append(desig)

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.figure()
print (totalTopicResults)
plt.hist(totalTopicResults, bins=10)
plt.xlabel('topic index')
plt.ylabel('data number')
plt.savefig('topic_distribution.pdf', format='pdf')

