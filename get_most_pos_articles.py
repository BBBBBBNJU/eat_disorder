import pickle
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from random import shuffle
from pu import PUAdapter
import gensim
from sklearn.metrics import euclidean_distances
from pyemd import emd
'''Get reddit posts with highest positive probability/scores via different methods
'''
vectorizer = TfidfVectorizer(ngram_range=(1,2), decode_error='ignore')
clf = LogisticRegression(class_weight='balanced')
neg_stem = pickle.load(open('neg_stem','rb'))
pos_stem = pickle.load(open('pos_stem','rb'))
stem_doc_dict = pickle.load(open('stem_doc_dict','rb'))
# use pos and neg
test_doc_list = []
for key, doc in stem_doc_dict.iteritems():
    test_doc_list.append(doc)
# use labeled data
train_label = [1] * len(pos_stem) + [0] * len(neg_stem)
train_vec = vectorizer.fit_transform(pos_stem + neg_stem).toarray()
test_vec = vectorizer.transform(test_doc_list).toarray()
clf.fit(train_vec, train_label)
temp_result = clf.predict_proba(test_vec)
result_dict = {}
for i, eachone in enumerate(temp_result):
    result_dict[i] = eachone[1]
top_keys = []
for key, value in sorted(result_dict.iteritems(), key=lambda (k,v): (v,k), reverse=True):
    top_keys.append(key)
pos_neg_result_dict = {}
for rank, idxList in enumerate(top_keys):
    stem_text = test_doc_list[idxList]
    for key, doc in stem_doc_dict.iteritems():
        if stem_text == doc:
            pos_neg_result_dict[key] = rank
pickle.dump(pos_neg_result_dict, open('pos_neg_result_dict', 'wb'))
print("finish pos+neg")

# use 6k data
train_label = [1] * len(pos_stem) + [0] * len(test_doc_list)
train_vec = vectorizer.fit_transform(pos_stem + test_doc_list).toarray()
test_vec = vectorizer.transform(test_doc_list).toarray()
clf.fit(train_vec, train_label)
temp_result = clf.predict_proba(test_vec)
result_dict = {}
for i, eachone in enumerate(temp_result):
    result_dict[i] = eachone[1]
top_keys = []
for key, value in sorted(result_dict.iteritems(), key=lambda (k,v): (v,k), reverse=True):
    top_keys.append(key)
pos_6k_result_dict = {}
for rank, idxList in enumerate(top_keys):
    stem_text = test_doc_list[idxList]
    for key, doc in stem_doc_dict.iteritems():
        if stem_text == doc:
            pos_6k_result_dict[key] = rank
pickle.dump(pos_6k_result_dict, open('pos_6k_result_dict', 'wb'))
print("finish pos+6k")
#
# use pu method
pu_adapt = PUAdapter(clf, hold_out_ratio=0.2)
train_label = [1] * len(pos_stem) + [0] * len(test_doc_list)
train_vec = vectorizer.fit_transform(pos_stem + test_doc_list).toarray()
test_vec = vectorizer.transform(test_doc_list).toarray()
result_dict = {}
for i in range(20):
    result_dict[i] = []
    print i
    pu_adapt.fit(train_vec, train_label)
    print pu_adapt
    temp_result = pu_adapt.predict_proba(test_vec)
    for j, eachone in enumerate(temp_result):
        result_dict[i].append(eachone)
ave_result_dict = {}
for i in range(len(test_doc_list)):
    temp_vec = []
    for j in range(20):
        temp_vec.append(result_dict[j][i])
    ave_result_dict[i] = np.median(temp_vec)

top_keys = []
for key, value in sorted(ave_result_dict.iteritems(), key=lambda (k,v): (v,k), reverse=True):
    top_keys.append(key)
pu_result_dict = {}
for rank, idxList in enumerate(top_keys):
    stem_text = test_doc_list[idxList]
    for key, doc in stem_doc_dict.iteritems():
        if stem_text == doc:
            pu_result_dict[key] = rank
pickle.dump(pu_result_dict, open('pu_result_dict', 'wb'))
print("finish pu")

# use wmd
pos_w2v = pickle.load(open('pos_w2v','rb'))
w2v_doc_dict = pickle.load(open('w2v_doc_dict','rb'))
test_doc_list = []
for key, doc in w2v_doc_dict.iteritems():
    test_doc_list.append(doc)
model = gensim.models.KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True)
vec_size = 300

def wmd(unlabel, pos):
    vect = CountVectorizer(stop_words="english").fit([unlabel, pos])
    v_1, v_2 = vect.transform([unlabel, pos])
    v_1 = v_1.toarray().ravel()
    v_2 = v_2.toarray().ravel()
    v_1 = v_1.astype(np.double)
    v_2 = v_2.astype(np.double)
    v_1 /= v_1.sum()
    v_2 /= v_2.sum()
    W_ = [model[w] for w in vect.get_feature_names()]
    D_ = euclidean_distances(W_)
    D_ = D_.astype(np.double)
    result = emd(v_1, v_2, D_)
    return result

results_mat = np.zeros((len(test_doc_list), len(pos_w2v)))

for i in range(len(test_doc_list)):
    for j in range(len(pos_w2v)):
        results_mat[i][j] = wmd(test_doc_list[i], pos_w2v[j])
    print('finish :' + str(i))
pickle.dump(results_mat, open('results_mat_wmd','wb'))
result_dict = {}
for i, eachresult in enumerate(results_mat):
    result_dict[i] = np.min(eachresult)
    # temp_result = sorted(eachresult, reverse=False)
    # try:
    #     result_dict[i] = np.mean(temp_result[0:5])
    # except:
    #     print i
top_keys = []
for key, value in sorted(result_dict.iteritems(), key=lambda (k,v): (v,k), reverse=False):
    top_keys.append(key)
wmd_result_dict = {}
for rank, idxList in enumerate(top_keys):
    w2v_text = test_doc_list[idxList]
    for key, doc in w2v_doc_dict.iteritems():
        if w2v_text == doc:
            wmd_result_dict[key] = rank
pickle.dump(wmd_result_dict, open('wmd_result_dict', 'wb'))
print("finish wmd")


