import numpy as np
import cPickle
from sklearn import metrics
from sklearn.metrics import roc_auc_score
from scipy import stats
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import re
from sklearn.svm import SVR
import os
import numpy as np
import scipy
import cPickle
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import scipy.io as sio
from sklearn.linear_model import LogisticRegression
from numpy.random import permutation
from random import shuffle
from sklearn import metrics
import sys
from gensim import corpora, models
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize
from nltk.stem.porter import PorterStemmer
import re
'''run LDA model
'''
space = ' '
from stop_words import get_stop_words

en_stop = get_stop_words('en')
p_stemmer = PorterStemmer()
htmlentities = ["&quot;","&nbsp;","&amp;","&lt;","&gt;","&OElig;","&oelig;","&Scaron;","&scaron;","&Yuml;","&circ;","&tilde;","&ensp;","&emsp;","&thinsp;","&zwnj;","&zwj;","&lrm;","&rlm;","&ndash;","&mdash;","&lsquo;","&rsquo;","&sbquo;","&ldquo;","&rdquo;","&bdquo;","&dagger;","&Dagger;","&permil;","&lsaquo;"]
# validElement = [',','.','"','\'']

def process(original_text):
    clean_text = ""
    for eachone in original_text:
        try:
            eachone.encode('ascii')
            clean_text += eachone
        except:
            pass
    return clean_text

def sentencePreProcess(raw):
    raw = raw.encode('ascii','ignore')
    for h in htmlentities:
        raw = raw.replace(h, " ")
    tokens = word_tokenize(raw.lower())
    stopped_tokens = [j for j in tokens if not j in en_stop]
    valid_tokens = [j for j in stopped_tokens if (j.isalpha())]
    stemmed_tokens = [p_stemmer.stem(j) for j in valid_tokens]
    return space.join(stemmed_tokens)

def preProcess(plainText):
    plainText = re.sub(u'``',' " ',plainText)
    plainText = re.sub(u'\'\'',' " ',plainText)
    plainText = re.sub(u'\|','',plainText)
    allsentences = sent_tokenize(plainText)
    afterProcess = ""
    for i in range(len(allsentences)):
        if i != len(allsentences)-1:
            tempSentence = sentencePreProcess(allsentences[i])
            afterProcess += tempSentence + ' '
    return afterProcess


subreddit_list = ['EatingDisorders',
                  'BingeEatingDisorder',
                  'eating_disorders',
                  'bulimia',
                  'proED',
                  'fuckeatingdisorders'
                  ]
totalArticle = []
for eachsub in subreddit_list:
    file_loc = './submissions/'+eachsub+'.txt'
    reader = open(file_loc,'r')
    all_text = ''
    for eachline in reader:
        all_text += eachline
    all_subs = all_text.split('=====')
    for eachsub in all_subs:
        clean_sub = preProcess(process(eachsub))
        totalArticle.append(clean_sub)


totalArticle_token = [eacharticle.split() for eacharticle in totalArticle]

dictionary = corpora.Dictionary(totalArticle_token)
corpus = [dictionary.doc2bow(article) for article in totalArticle_token]
LDA_model = models.ldamulticore.LdaMulticore(corpus, num_topics=10, id2word=dictionary, passes=20)

print(LDA_model.print_topics(num_topics=10, num_words=10))
cPickle.dump(LDA_model, open('lda_model_6k','wb'))
cPickle.dump(dictionary, open('lda_dic_6k', 'wb'))
