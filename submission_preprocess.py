from nltk.tokenize import sent_tokenize
'''remove urls in original submission.
'''
subreddit_list = ['EatingDisorders',
                  'BingeEatingDisorder',
                  'eating_disorders',
                  'bulimia',
                  'proED',
                  'fuckeatingdisorders'
                  ]
lowerArticle = []
for eachsub in subreddit_list:
    file_loc = './submissions/'+eachsub+'.txt'
    reader = open(file_loc,'r')
    writer = open('./submissions_remove_url/'+eachsub+'.txt', 'w')
    writer.write('=====\n')
    all_text = ''
    for eachline in reader:
        all_text += eachline.strip() + ' '
    all_subs = all_text.split('=====')
    all_subs = list(set(all_subs))
    for eachsub in all_subs:
        allSentences = sent_tokenize(eachsub)
        afterRemoveUrl = ""
        for eachSentence in allSentences:
            if 'http' not in eachSentence.lower():
                afterRemoveUrl += eachSentence + ' '
        if len(afterRemoveUrl) > 5:
            writer.write(afterRemoveUrl + '\n=====\n')
    writer.close()


