import pickle
'''print second round classification results
'''

url_dict = pickle.load(open('url_dict','rb'))
total_rank_dict = {}
total_sub = set()

pos_neg_result_dict = pickle.load(open('pos_neg_2ng_round_result_dict','rb'))
for article , rank in pos_neg_result_dict.iteritems():
    if rank < 50:
        total_sub.add(article)


wmd_result_dict = pickle.load(open('wmd_result_dict_second_round','rb'))
for article , rank in wmd_result_dict.iteritems():
    if rank < 50:
        total_sub.add(article)

for eachartcie in total_sub:
    templist = [-1] * 2
    if eachartcie in pos_neg_result_dict:
        templist[0] = pos_neg_result_dict[eachartcie]
    else:
        templist[0] = -1
    if eachartcie in wmd_result_dict:
        templist[1] = wmd_result_dict[eachartcie]
    else:
        templist[1] = -1
    total_rank_dict[eachartcie] = templist

writer = open('total_rank_2nd_round.csv', 'w')
writer.write('submission, pos vs. neg, wmd (top 5), url\n')
for eachartcie, rankinfo in total_rank_dict.iteritems():
    tempUrl = url_dict[eachartcie]
    writer.write('\''+eachartcie.strip().replace(',','_').replace('\n',' ')+'\','
                 + str(rankinfo[0]) + ','
                 + str(rankinfo[1]) + ','
                 + tempUrl + '\n'
                 )
writer.close()
