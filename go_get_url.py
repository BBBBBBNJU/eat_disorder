import praw
import sys
import io
import pickle
import pprint
'''remove url in original post
'''
# def process(original_text):
#     clean_text = ""
#     for eachone in original_text:
#         try:
#             eachone.encode('ascii')
#             clean_text += eachone
#         except:
#             pass
#     return clean_text
#
# subreddit_list = ['EatingDisorders',
#                   'BingeEatingDisorder',
#                   'eating_disorders',
#                   'bulimia',
#                   'proED',
#                   'fuckeatingdisorders'
#                   ]
#
# reddit = praw.Reddit(user_agent='Comment Extraction',
#                      client_id='BbRqRc0i664rrQ',
#                      client_secret="7qPpzsYzgngcIHqmN9_p_aVmyVk"
#                      )
# raw_article_url_dict = {}
# for eachsubred in subreddit_list:
#     subreddit = reddit.subreddit(eachsubred)
#     for submission in subreddit.hot(limit=10000):
#         clean_text = process(submission.selftext)
#         if len(clean_text) > 0:
#             raw_article_url_dict[clean_text] = submission.url
# pickle.dump(raw_article_url_dict, open('article_url_dict','wb'))

from nltk.tokenize import sent_tokenize

old_article_url_dict = pickle.load(open('article_url_dict','rb'))
new_article_url_dict = {}
for old_article,url in old_article_url_dict.iteritems():
        allSentences = sent_tokenize(old_article)
        afterRemoveUrl = ""
        for eachSentence in allSentences:
            if 'http' not in eachSentence.lower():
                afterRemoveUrl += eachSentence + ' '
        if len(afterRemoveUrl) > 5:
            new_article_url_dict[afterRemoveUrl] = url
pickle.dump(new_article_url_dict, open('new_artcile_url_dict','wb'))

