import numpy as np
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from nltk.stem.porter import PorterStemmer
'''Spy method.
'''
space = ' '
from stop_words import get_stop_words
import re

en_stop = get_stop_words('en')
p_stemmer = PorterStemmer()
htmlentities = ["&quot;","&nbsp;","&amp;","&lt;","&gt;","&OElig;","&oelig;","&Scaron;","&scaron;","&Yuml;","&circ;","&tilde;","&ensp;","&emsp;","&thinsp;","&zwnj;","&zwj;","&lrm;","&rlm;","&ndash;","&mdash;","&lsquo;","&rsquo;","&sbquo;","&ldquo;","&rdquo;","&bdquo;","&dagger;","&Dagger;","&permil;","&lsaquo;"]
validElement = [',','.','"','\'']

def process(original_text):
    clean_text = ""
    for eachone in original_text:
        try:
            eachone.encode('ascii')
            clean_text += eachone
        except:
            pass
    return clean_text

def sentencePreProcess(raw):
    raw = raw.encode('ascii','ignore')
    for h in htmlentities:
        raw = raw.replace(h, " ")
    tokens = word_tokenize(raw.lower())
    stopped_tokens = [j for j in tokens if not j in en_stop]
    valid_tokens = [j for j in stopped_tokens if (j.isalpha() or j in validElement)]
    stemmed_tokens = [p_stemmer.stem(j) for j in valid_tokens]
    return space.join(stemmed_tokens)

def preProcess(plainText):
    plainText = re.sub(u'``',' " ',plainText)
    plainText = re.sub(u'\'\'',' " ',plainText)
    plainText = re.sub(u'\|','',plainText)
    allsentences = sent_tokenize(plainText)
    afterProcess = ""
    for i in range(len(allsentences)):
        if i != len(allsentences)-1:
            tempSentence = sentencePreProcess(allsentences[i])
            afterProcess += tempSentence + ' '
    return afterProcess

# load positive data
reader = open('labeled_reddit_pos.txt','r')
all_text = ''
for eachline in reader:
    all_text += eachline
all_subs = all_text.split('====')
pos = []
for eachsub in all_subs:
    clean_sub = preProcess(process(eachsub))
    pos.append(clean_sub)

subreddit_list = ['EatingDisorders',
                  'BingeEatingDisorder',
                  'eating_disorders',
                  'bulimia',
                  'proED',
                  'fuckeatingdisorders'
                  ]

submissions_6k = []
submissions_raw = []
for eachsub in subreddit_list:
    file_loc = './submissions_remove_url/'+eachsub+'.txt'
    reader = open(file_loc,'r')
    all_text = ''
    for eachline in reader:
        all_text += eachline
    all_subs = all_text.split('=====')
    for eachsub in all_subs:
        if len(eachsub) > 10:
            after_Process = preProcess(eachsub).lower()
            if len(after_Process) > 10:
                submissions_6k.append(after_Process)
                submissions_raw.append(eachsub)
from random import shuffle
vectorizer = TfidfVectorizer(ngram_range=(1,2), decode_error='ignore')
clf = LogisticRegression(class_weight='balanced')
result_dict = {}
for i in range(20):
    print i
    result_dict[i] = []
    shuffle(pos)
    PS = pos[10:None]
    NG = pos[0:10] + submissions_6k

    train_label = [1] * len(PS) + [0] * len(NG)
    train_vec = vectorizer.fit_transform(PS + NG).toarray()
    test_vec = vectorizer.transform(NG).toarray()
    clf.fit(train_vec, train_label)
    temp_result = clf.predict_proba(test_vec)
    for j, eachone in enumerate(temp_result):
        if j >= 10:
            result_dict[i].append(eachone[1])

ave_result_dict = {}
for i in range(len(submissions_6k)):
    temp_vec = []
    for j in range(20):
        temp_vec.append(result_dict[j][i])
    ave_result_dict[i] = np.median(temp_vec)

from collections import deque
top_keys = deque(maxlen=10000)
for key, value in sorted(ave_result_dict.iteritems(), key=lambda (k,v): (v,k), reverse=True):
    top_keys.append(key)

writer = open('top_all_pu_based.txt','w')
for eachkey in top_keys:
    writer.write('======\n')
    writer.write(submissions_raw[eachkey]+'\n')
writer.close()
