import mysql.connector
from random import shuffle
'''load post to mysql db
'''
count = 0
submissions = []
pos_6k = []
pos_neg = []
pu_method = []
wmd_5 = []
wmd_1 = []
urls = []
reader = open('total_rank.csv', 'r')
for eachline in reader:
    if count == 0:
        count += 1
    else:
        items = eachline.strip().split(',')
        submissions.append(items[0].replace('_',','))
        pos_6k.append(int(items[1]))
        pos_neg.append(int(items[2]))
        pu_method.append(int(items[3]))
        wmd_5.append(int(items[4]))
        wmd_1.append(int(items[5]))
        urls.append(items[6])
total_data =  list(zip(submissions, pos_6k, pos_neg, pu_method, wmd_5, wmd_1, urls))
shuffle(total_data)
config = {
    'user': 'root',
    'password': 'PASSWORD',
    'host': 'localhost',
    'database': 'all_top_50'
}
cnx = mysql.connector.connect(**config)
cur = cnx.cursor()
delete_item = ("DELETE FROM all_submissions ")
cur.execute(delete_item)
cnx.commit()
add_item = ("INSERT INTO all_submissions "
            "(idx, submission, pos_6k, pos_neg, pu_method, wmd_5, wmd_1, url, label) "
            "VALUES (%(idx)s, %(sub)s, %(p6)s, %(pn)s, %(pm)s, %(w5)s, %(w1)s, %(url)s, %(label)s)")

idx = 0
for sub, p6, pn, pm, w5, w1, url in total_data:
    item = {
        'idx': idx,
        'sub': sub,
        'p6': p6,
        'pn': pn,
        'pm': pm,
        'w5': w5,
        'w1': w1,
        'url': url,
        'label': "",
    }
    cur.execute(add_item, item)
    idx += 1
cnx.commit()
cur.close()
cnx.close()

