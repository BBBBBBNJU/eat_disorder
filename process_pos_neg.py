import pickle
import praw
import re
import gensim
import pprint
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer

'''Proprocess original labeled dataset.
'''
p_stemmer = PorterStemmer()
en_stop = get_stop_words('en')
htmlentities = ["&quot;","&nbsp;","&amp;","&lt;","&gt;","&OElig;","&oelig;","&Scaron;","&scaron;","&Yuml;","&circ;","&tilde;","&ensp;","&emsp;","&thinsp;","&zwnj;","&zwj;","&lrm;","&rlm;","&ndash;","&mdash;","&lsquo;","&rsquo;","&sbquo;","&ldquo;","&rdquo;","&bdquo;","&dagger;","&Dagger;","&permil;","&lsaquo;"]
model = gensim.models.KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True)

def process_1(original_text):
    clean_text = ""
    for eachone in original_text:
        try:
            eachone.encode('ascii')
            clean_text += eachone
        except:
            pass
    return clean_text

def sentencePreProcess(raw):
    raw = raw.encode('ascii','ignore')
    for h in htmlentities:
        raw = raw.replace(h, " ")
    tokens = word_tokenize(raw.lower())
    stopped_tokens = [j for j in tokens if not j in en_stop]
    valid_tokens = [j for j in stopped_tokens if (j.isalpha())]
    return ' '.join(valid_tokens)

def process_2(plainText):
    plainText = re.sub(u'``',' " ',plainText)
    plainText = re.sub(u'\'\'',' " ',plainText)
    plainText = re.sub(u'\|','',plainText)
    allsentences = sent_tokenize(plainText)
    afterProcess = ""
    for i in range(len(allsentences)):
        tempSentence = sentencePreProcess(allsentences[i])
        afterProcess += tempSentence + ' '
    return afterProcess

def w2v_model_token(raw):
    tokens = word_tokenize(raw)
    valid_tokens = []
    for token in tokens:
        try:
            model[token]
            valid_tokens.append(token)
        except:
            pass
    return ' '.join(valid_tokens)

def stem_token(raw):
    tokens = word_tokenize(raw)
    valid_tokens = []
    for token in tokens:
        try:
            valid_tokens.append(p_stemmer.stem(token))
        except:
            pass
    return ' '.join(valid_tokens)
# load positive data

reader = open('labeled_reddit_pos.txt','r')
all_text = ''
for eachline in reader:
    all_text += eachline
all_subs = all_text.split('====')
pos_w2v = []
pos_stem = []
for eachsub in all_subs:
    # process1, get rid of non-ascii stuff
    clean_text_1 = process_1(eachsub)
    # process2, get rid of stop words, lower but not stem
    clean_text = process_2(clean_text_1)
    # process3, retain tokens that in Word2Vec
    w2v_doc = w2v_model_token(clean_text)
    # process4, retain stem
    stem_doc = stem_token(clean_text)
    if len(stem_doc) > 10 and len(w2v_doc) > 10:
        pos_w2v.append(w2v_doc)
        pos_stem.append(stem_doc)
pickle.dump(pos_w2v, open('pos_w2v','wb'))
pickle.dump(pos_stem, open('pos_stem','wb'))

# load positive data
reader = open('labeled_reddit_neg.txt','r')
all_text = ''
for eachline in reader:
    all_text += eachline
all_subs = all_text.split('====')
neg_w2v = []
neg_stem = []
for eachsub in all_subs:
    # process1, get rid of non-ascii stuff
    clean_text_1 = process_1(eachsub)
    # process2, get rid of stop words, lower but not stem
    clean_text = process_2(clean_text_1)
    # process3, retain tokens that in Word2Vec
    w2v_doc = w2v_model_token(clean_text)
    # process4, retain stem
    stem_doc = stem_token(clean_text)
    if len(stem_doc) > 10 and len(w2v_doc) > 10:
        neg_w2v.append(w2v_doc)
        neg_stem.append(stem_doc)
pickle.dump(neg_w2v, open('neg_w2v','wb'))
pickle.dump(neg_stem, open('neg_stem','wb'))
