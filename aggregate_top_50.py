import pickle
'''We print reddit posts that rand top 50 in all methods.
'''
url_dict = pickle.load(open('url_dict','rb'))
total_rank_dict = {}
total_sub = set()
pos_6k_result_dict = pickle.load(open('pos_6k_result_dict','rb'))
for article , rank in pos_6k_result_dict.iteritems():
    if rank < 50:
        total_sub.add(article)

pos_neg_result_dict = pickle.load(open('pos_neg_result_dict','rb'))
for article , rank in pos_neg_result_dict.iteritems():
    if rank < 50:
        total_sub.add(article)

pu_result_dict = pickle.load(open('pu_result_dict','rb'))
for article , rank in pu_result_dict.iteritems():
    if rank < 50:
        total_sub.add(article)

wmd_result_dict = pickle.load(open('wmd_result_dict','rb'))
for article , rank in wmd_result_dict.iteritems():
    if rank < 50:
        total_sub.add(article)

wmd_result_dict_1 = pickle.load(open('wmd_result_dict_1','rb'))
for article , rank in wmd_result_dict_1.iteritems():
    if rank < 50:
        total_sub.add(article)

for eachartcie in total_sub:
    templist = [-1] * 5
    templist[0] = pos_6k_result_dict[eachartcie]
    templist[1] = pos_neg_result_dict[eachartcie]
    templist[2] = pu_result_dict[eachartcie]
    templist[3] = wmd_result_dict[eachartcie]
    templist[4] = wmd_result_dict_1[eachartcie]
    total_rank_dict[eachartcie] = templist

writer = open('total_rank.csv', 'w')
writer.write('submission, pos vs. 6k, pos vs. neg, pu method, wmd (top 5), wmd (top 1), url\n')
for eachartcie, rankinfo in total_rank_dict.iteritems():
    tempUrl = url_dict[eachartcie]
    writer.write('\''+eachartcie.strip().replace(',','_').replace('\n',' ')+'\','
                 + str(rankinfo[0]) + ','
                 + str(rankinfo[1]) + ','
                 + str(rankinfo[2]) + ','
                 + str(rankinfo[3]) + ','
                 + str(rankinfo[4]) + ','
                 + tempUrl + '\n'
                 )
writer.close()

