import pickle
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from random import shuffle
from pu import PUAdapter
import gensim
from sklearn.metrics import euclidean_distances
from pyemd import emd
'''Second round classification.
'''
# use wmd
pos_w2v = pickle.load(open('pos_w2v','rb'))
w2v_doc_dict = pickle.load(open('w2v_doc_dict','rb'))
test_doc_list = []
for key, doc in w2v_doc_dict.iteritems():
    test_doc_list.append(doc)
model = gensim.models.KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True)
vec_size = 300

def wmd(unlabel, pos):
    vect = CountVectorizer(stop_words="english").fit([unlabel, pos])
    v_1, v_2 = vect.transform([unlabel, pos])
    v_1 = v_1.toarray().ravel()
    v_2 = v_2.toarray().ravel()
    v_1 = v_1.astype(np.double)
    v_2 = v_2.astype(np.double)
    v_1 /= v_1.sum()
    v_2 /= v_2.sum()
    W_ = [model[w] for w in vect.get_feature_names()]
    D_ = euclidean_distances(W_)
    D_ = D_.astype(np.double)
    result = emd(v_1, v_2, D_)
    return result

original_results_mat = np.zeros((len(test_doc_list), len(pos_w2v)))

for i in range(len(test_doc_list)):
    for j in range(len(pos_w2v)):
        original_results_mat[i][j] = wmd(test_doc_list[i], pos_w2v[j])
    print('origin finish :' + str(i))

result_dict = {}
for i, eachresult in enumerate(original_results_mat):
    temp_result = sorted(eachresult, reverse=False)
    try:
        result_dict[i] = np.mean(temp_result[0:5])
    except:
        print i

top_50_keys = []
for key, value in sorted(result_dict.iteritems(), key=lambda (k,v): (v,k), reverse=False):
    top_50_keys.append(key)
    if len(top_50_keys) == 50:
        break

train_set = pos_w2v
test_set = []
for idx, doc in enumerate(test_doc_list):
    if idx in top_50_keys:
        train_set.append(doc)
    else:
        test_set.append(doc)

new_results_mat = np.zeros((len(test_set), len(train_set)))

for i in range(len(test_set)):
    for j in range(len(train_set)):
        new_results_mat[i][j] = wmd(test_set[i], train_set[j])
    print('new finish :' + str(i))
pickle.dump(new_results_mat, open('new_results_mat','wb'))

result_dict = {}
for i, eachresult in enumerate(new_results_mat):
    temp_result = sorted(eachresult, reverse=False)
    try:
        result_dict[i] = np.mean(temp_result[0:5])
    except:
        print i

top_keys = []
for key, value in sorted(result_dict.iteritems(), key=lambda (k,v): (v,k), reverse=False):
    top_keys.append(key)

wmd_result_dict = {}
for rank, idxList in enumerate(top_keys):
    w2v_text = test_set[idxList]
    for key, doc in w2v_doc_dict.iteritems():
        if w2v_text == doc:
            wmd_result_dict[key] = rank
pickle.dump(wmd_result_dict, open('wmd_result_dict_second_round', 'wb'))
print("finish wmd")


# pos vs neg
vectorizer = TfidfVectorizer(ngram_range=(1,2), decode_error='ignore')
clf = LogisticRegression(class_weight='balanced')
neg_stem = pickle.load(open('neg_stem','rb'))
pos_stem = pickle.load(open('pos_stem','rb'))
stem_doc_dict = pickle.load(open('stem_doc_dict','rb'))
# use pos and neg
test_doc_list = []
for key, doc in stem_doc_dict.iteritems():
    test_doc_list.append(doc)
# use labeled data
train_label = [1] * len(pos_stem) + [0] * len(neg_stem)
train_vec = vectorizer.fit_transform(pos_stem + neg_stem).toarray()
test_vec = vectorizer.transform(test_doc_list).toarray()
clf.fit(train_vec, train_label)
temp_result = clf.predict_proba(test_vec)
result_dict = {}
for i, eachone in enumerate(temp_result):
    result_dict[i] = eachone[1]
top_50_keys = []
for key, value in sorted(result_dict.iteritems(), key=lambda (k,v): (v,k), reverse=True):
    top_50_keys.append(key)
    if len(top_50_keys) == 50:
        break
pos_set_new = pos_stem
neg_set_new = neg_stem
test_set_new = []
for idx, doc in enumerate(test_doc_list):
    if idx in top_50_keys:
        pos_set_new.append(doc)
    else:
        test_set_new.append(doc)

train_vec = vectorizer.fit_transform(pos_set_new+neg_set_new).toarray()
test_vec = vectorizer.transform(test_set_new).toarray()
train_label = [1] * len(pos_set_new) + [0] * len(neg_set_new)
clf.fit(train_vec, train_label)
temp_result = clf.predict_proba(test_vec)
result_dict = {}
for i, eachone in enumerate(temp_result):
    result_dict[i] = eachone[1]

top_keys = []
for key, value in sorted(result_dict.iteritems(), key=lambda (k,v): (v,k), reverse=True):
    top_keys.append(key)

pos_neg_2ng_round_result_dict = {}
for rank, idxList in enumerate(top_keys):
    stem_text = test_set_new[idxList]
    for key, doc in stem_doc_dict.iteritems():
        if stem_text == doc:
            pos_neg_2ng_round_result_dict[key] = rank
pickle.dump(pos_neg_2ng_round_result_dict, open('pos_neg_2ng_round_result_dict', 'wb'))
print("finish pos_neg_2ng_round_result_dict")



