import mysql.connector
import sys
'''Get the submissions that labeled as positive
'''
target_name = sys.argv[1]

config = {
    'user': 'root',
    'password': 'PASSWORD',
    'host': 'localhost',
    'database': 'all_top_50'
}
cnx = mysql.connector.connect(**config)
cur = cnx.cursor()
find_item = ("SELECT sub_idx, sub_label FROM all_label WHERE username='" + target_name + "'")
cur.execute(find_item)
target_idxs = []
target_labels = []
for (target_idx, target_label) in cur:
    target_labels.append(target_label)
    target_idxs.append(target_idx)

cnx.commit()
writer = open('error_rank_'+target_name+'.csv', 'w')
writer.write('idx, pos_6k, pos_neg, pu_method, wmd_5, wmd_1\n')
for i in range(len(target_labels)):
    if target_labels[i] == 0:
        temp_idx = str(target_idxs[i])
        temp_line = temp_idx + ','
        find_item = ("SELECT pos_6k, pos_neg, pu_method, wmd_5, wmd_1 FROM all_submissions WHERE idx='" + temp_idx + "'")
        cur.execute(find_item)
        for (pos_6k, pos_neg, pu_method, wmd_5, wmd_1) in cur:
            temp_line += str(pos_6k) + ','
            temp_line += str(pos_neg) + ','
            temp_line += str(pu_method) + ','
            temp_line += str(wmd_5) + ','
            temp_line += str(wmd_1) + '\n'
        writer.write(temp_line)

writer.close()
cur.close()
cnx.close()



